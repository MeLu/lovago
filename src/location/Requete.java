package location;

import java.sql.Date;
import java.util.List;

public class Requete {
	public Date date_debut;
	public int classe;
	public Agence agence;
	public Forfait forfait;
	
	public Requete(int classe,Forfait forfait,Date date_debut,Agence agence) {
		this.classe=classe;
		this.forfait=forfait;
		this.date_debut=date_debut;
		this.agence=agence;
	}
	
	
	public Forfait getForfait() {
		return forfait;
	}
	
	public Date getDate_debut() {
		return date_debut;
	}
	


	
	
	

}
