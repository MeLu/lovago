package location;

public class Voiture {
	public int classe;
	public Agence A;
	public String etat;
	public String voiture;
	public String[] options;
	public int kilometrage;
	
	public Voiture(int classe, String etat, String[] options, String voiture, Agence A, int kilometrage) {//constructeur de l'objet Voiture
		this.voiture=voiture;
		this.etat=etat;
		this.A=A;
		this.options= options;
		this.classe= classe;	
		this.kilometrage=kilometrage;
	}
	
	public String getEtat() {
		return etat;
	}
	
	public void setEtat(String etat) {
		this.etat= etat;
	}
	
	}
