package location;

public enum Etat {
	LOUEE(1), PAS_LOUEE(2),A_REPARER(3),EN_REPRATION(4);
	
	private int codeEtat;
	
	Etat(int codeEtat){
		this.codeEtat=codeEtat;
	}
	
	public int getCodeEtat() {
		return codeEtat;
	}
	
}
