package location;

public class Forfait {
	private int km;
	private int duree;
	private String forfait;
	private int classe;
	private double prix;
	private static double hors_forfait_km;
	private static double hors_forfait_duree;
	
	


	public  Forfait(int km,int duree,int classe,int hors_forfait_duree,int hors_forfait_km,int prix){
		//constructeur de l'objet Forfait
		this.setKm(km);
		this.setDuree(duree);
		this.classe = classe;
		Forfait.hors_forfait_duree = hors_forfait_duree;
		Forfait.hors_forfait_km = hors_forfait_km;
		this.prix = prix;
		}
	
	public int getKm() {
		return km;
	}


	public void setKm(int km) {
		this.km = km;
	}
	
	public int getDuree() {
		return duree;
	}

	public void setDuree(int duree) {
		this.duree = duree;
	}

	public String getForfait() {
		return forfait;
	}

	public void setForfait(String forfait) {
		this.forfait = forfait;
	}
	

	

	



}
