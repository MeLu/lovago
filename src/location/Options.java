package location;

public enum Options {
	BOITEAUTOMATIQUE(10),INTERIEURCUIR(40),
	JANTESALLIAGES(10),CLIMATISATION(2),COMMANDESVOCALES(5),CHROMESDECARROSSERIE(15),
	TOITPANORAMIQUE(20),BOISPRECIEUX(50), ORDINATEURDEBORD(10),GPS(5);
	
	public int prix;
	
	Options(int prix){
		//constructeur de l'énumération prix
		this.prix=prix;
	}
	
	public int getPrix() {
		return prix;
	}
	
	

}
